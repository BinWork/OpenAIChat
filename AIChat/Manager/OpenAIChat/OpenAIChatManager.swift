//
//  OpenAIChatManager.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import Foundation

private var apiKey: String? {
    if let filePath = Bundle.main.path(forResource: "OpenAIAPIKey", ofType: "txt"),
       let apiKey = try? String(contentsOfFile: filePath, encoding: .utf8) {
        return apiKey
    } else {
        return nil
    }
}

private var sessionURL: URL {
    return URL(string: Bundle.main.infoDictionary?["BaseURL"] as! String)!
}

private var chatModel: OpenAIChatModel {
    return .gpt3_5_turbo
}

class OpenAIChatManager {
    static let `default` = OpenAIChatManager()
    
    var currentModel: OpenAIChatModel { chatModel }
    var isAPIKeyExist: Bool { !apiKey.isNilOrEmpty }
    
    func submitMessages(_ messages: [Message]) async throws -> Message {
        let request = OpenAIRequest(model: chatModel.string, messages: messages).create()
        do {
            let response: OpenAIResponse = try await fetchData(request: request)
            guard let message = response.choices.first?.message else {
                throw OpenAIError.emptyMessage
            }
            return message
        } catch {
            throw error
        }
    }
}

private extension OpenAIChatManager {
    private func fetchData<T: Decodable>(request: URLRequest) async throws -> T {
        let (data, _) = try await URLSession.shared.data(for: request)
        if let json = try? JSONDecoder().decode(T.self, from: data) {
            return json
        }
        let errorResposne = try JSONDecoder().decode(OpenAPIErrorResponse.self, from: data)
        throw errorResposne.error
    }
}

private extension OpenAIRequest {
    func create() -> URLRequest {
        var urlRequest = URLRequest(url: sessionURL)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if let apiKey {
            urlRequest.setValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        }
        urlRequest.httpMethod = "POST"
        let jsonData = try! JSONEncoder().encode(self)
        urlRequest.httpBody = jsonData
        
        return urlRequest
    }
}
