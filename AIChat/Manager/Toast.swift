//
//  Toast.swift
//  AIChat
//
//  Created by bin on 2023/4/8.
//

import Foundation
import UIKit

class Toast {
    public private(set) var message: String
    
    init(message: String) {
        self.message = message
    }
}

class ToastManager {
    static let shared = ToastManager()
    
    private var toastQueue: [Toast] = []
    private var isAnimating: Bool = false

    func addToastMessage(_ message: String) {
        let toast = Toast(message: message)
        showToast(toast)
    }
    
    func showToast(_ toast: Toast) {
    toastQueue.append(toast)
        if !isAnimating {
            showNextToast()
        }
    }
    
    private func showNextToast() {
        guard !toastQueue.isEmpty else {
            isAnimating = false
            return
        }
        isAnimating = true
        let toastView = ToastView(toast: toastQueue.removeFirst())
        toastView.addToView()
        UIView.animate(withDuration: 0.3, animations: {
            toastView.show()
        }) { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                UIView.animate(withDuration: 0.3, animations: {
                    toastView.hide()
                }) { [weak self] _ in
                    guard let strongSelf = self else { return }
                    toastView.removeFromSuperview()
                    strongSelf.showNextToast()
                }
            }
        }
    }
}

private class ToastView: UIView {

    let toast: Toast
    
    private lazy var messageLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = self.toast.message
        lbl.numberOfLines = 0
        lbl.textColor = .primaryText
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    init(toast: Toast) {
        self.toast = toast
        super.init(frame: .zero)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureUI() {
        translatesAutoresizingMaskIntoConstraints = false
        makeCorners()
        
        addBlurView()
        
        let vStack = UIStackView()
        vStack.axis = .vertical
        vStack.alignment = .center
        vStack.translatesAutoresizingMaskIntoConstraints = false
        vStack.isLayoutMarginsRelativeArrangement = true
        vStack.directionalLayoutMargins = .init(top: 10, leading: 10, bottom: 10, trailing: 10)
        addSubview(vStack)
        NSLayoutConstraint.activate([
            vStack.topAnchor.constraint(equalTo: topAnchor),
            vStack.leadingAnchor.constraint(equalTo: leadingAnchor),
            vStack.trailingAnchor.constraint(equalTo: trailingAnchor),
            vStack.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        vStack.addArrangedSubview(messageLabel)
    }
    
    func addToView() {
        guard let scene = UIApplication.shared.connectedScenes.first(where: { $0.activationState == .foregroundActive }) else { return }
        if let presentedViewController = (scene as? UIWindowScene)?.windows.first?.rootViewController?.presentedViewController
            ?? (scene as? UIWindowScene)?.windows.first?.rootViewController,
           let presentedView = presentedViewController.view {

            let toastView = self
            presentedView.addSubview(toastView)
            NSLayoutConstraint.activate([
                toastView.leadingAnchor.constraint(greaterThanOrEqualTo: presentedView.leadingAnchor, constant: 30),
                toastView.trailingAnchor.constraint(lessThanOrEqualTo: presentedView.trailingAnchor, constant: -30),
                toastView.centerXAnchor.constraint(equalTo: presentedView.centerXAnchor),
                toastView.centerYAnchor.constraint(equalTo: presentedView.centerYAnchor)
            ])
        }
    }
    
    func show() {
        self.alpha = 1
    }
    
    func hide() {
        self.alpha = 0
    }
    
    private func addBlurView() {
        let blurEffect = UIBlurEffect(style: .prominent)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        insertSubview(blurView, at: 0)
        NSLayoutConstraint.activate([
            blurView.topAnchor.constraint(equalTo: topAnchor),
            blurView.leadingAnchor.constraint(equalTo: leadingAnchor),
            blurView.trailingAnchor.constraint(equalTo: trailingAnchor),
            blurView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
