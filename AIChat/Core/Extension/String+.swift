//
//  String+.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import Foundation

extension Optional where Wrapped == String {
    var isNilOrEmpty: Bool {
        guard let string = self else { return true }
        return string.isEmpty
    }
}

extension String {
    func localized() -> Self {
        self
    }
}
