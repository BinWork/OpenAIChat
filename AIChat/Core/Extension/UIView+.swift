//
//  UIView+.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import UIKit.UIView

extension UIView {
    func makeCorners(_ cornerRadius: CGFloat? = nil) {
        clipsToBounds = true
        layer.cornerRadius = cornerRadius ?? 10
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
    }
}
