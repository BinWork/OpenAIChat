//
//  UIImage+.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import UIKit.UIImage

extension UIImage {
    static var avatarBot: UIImage? {
        UIImage(named: "bot")
    }
    
    static var avatarUser: UIImage? {
        UIImage(named: "user")
    }
}
