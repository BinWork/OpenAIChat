//
//  UIScrollView+.swift
//  AIChat
//
//  Created by bin on 2023/3/27.
//

import UIKit.UIScrollView

extension UIScrollView {
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height)
        setContentOffset(bottomOffset, animated: true)
    }
}
