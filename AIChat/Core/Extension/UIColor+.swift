//
//  UIColor+.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import UIKit.UIColor

extension UIColor {
    static var primaryText: UIColor {
        UIColor(named: "PrimaryText")!
    }
    
    static var primaryBackground: UIColor {
        UIColor(named: "PrimaryBackground")!
    }
    
    static var primaryButton: UIColor {
        UIColor(named: "PrimaryButton")!
    }
    
    static var primaryMessageBackground: UIColor {
        UIColor(named: "PrimaryMessageBackground")!
    }
    
    static var secondaryMessageBackground: UIColor {
        UIColor(named: "SecondaryMessageBackground")!
    }
}
