//
//  OpenAIResponse.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import Foundation

struct OpenAIResponse: Decodable {
    let id: String
    let object: String
    let created: Int
    let model: String
    let usage: OpenAIUsage
    let choices: [OpenAIChoice]
    
    private enum CodingKeys: String, CodingKey {
        case id
        case object
        case created
        case model
        case usage
        case choices
    }
}

struct OpenAIChoice: Decodable {
    let index: Int
    let message: Message
    let finishReason: String?
    
    private enum CodingKeys: String, CodingKey {
        case index
        case message
        case finishReason = "finish_reason"
    }
}

struct OpenAIUsage: Decodable {
    let promptTokens: Int
    let completionTokens: Int
    let totalTokens: Int
    
    private enum CodingKeys: String, CodingKey {
        case promptTokens = "prompt_tokens"
        case completionTokens = "completion_tokens"
        case totalTokens = "total_tokens"
    }
}

public struct OpenAPIErrorResponse: Error, Decodable {
    public let error: OpenAPIError
}
