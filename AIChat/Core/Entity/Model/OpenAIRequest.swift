//
//  OpenAIRequest.swift
//  AIChat
//
//  Created by bin on 2023/3/28.
//

import Foundation

struct OpenAIRequest: Codable {
    let model: String
    let messages: [Message]
    
    private enum CodingKeys: String, CodingKey {
        case model
        case messages
    }
}
