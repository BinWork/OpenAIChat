//
//  OpenAIChatModel.swift
//  AIChat
//
//  Created by bin on 2023/3/16.
//

import Foundation

public enum OpenAIChatModel: Codable {
    case gpt3_5_turbo
    case text_davinci_003

    var string: String {
        switch self {
        case .gpt3_5_turbo: return "gpt-3.5-turbo"
        case .text_davinci_003: return "text-davinci-003"
        }
    }
}
