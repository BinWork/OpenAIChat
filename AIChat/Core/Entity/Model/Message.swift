//
//  Message.swift
//  AIChat
//
//  Created by bin on 2023/3/28.
//

import Foundation

struct Message: Codable {
    let role: MessageRole
    let content: String

    private enum CodingKeys: String, CodingKey {
        case role
        case content
    }
}

enum MessageRole: String, Codable {
    case assistant
    case system
    case user
    
    var isAssistant: Bool { self == .assistant }
    var isSystemt: Bool { self == .system }
    var isUser: Bool { self == .user }
}
