//
//  Error.swift
//  AIChat
//
//  Created by bin on 2023/3/28.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case invalidResponse
    case invalidData
    case decodingError
}

enum OpenAIError: Error {
    case error(Error)
    case emptyMessage
}

public struct OpenAPIError: Error, Decodable {
    public let message: String
    public let type: String
    public let param: String?
    public let code: String?
}
