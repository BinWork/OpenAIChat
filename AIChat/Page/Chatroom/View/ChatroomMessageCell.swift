//
//  ChatroomMessageCell.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import UIKit
import Combine

protocol GrowingCellProtocol: AnyObject {
    func updateHeightOfCell(_ cell: UITableViewCell, fit size: CGSize)
    func updateHeightCompleted(_ cell: UITableViewCell)
}

class ChatroomMessageCell: UITableViewCell {

    private var cancellables: Set<AnyCancellable> = []
    weak var cellDelegate: GrowingCellProtocol?
    
    private var message: Message? {
        didSet {
            if let message, message.role.isUser {
                roleView.image = .avatarUser
                contentView.backgroundColor = .primaryMessageBackground
            } else {
                roleView.image = .avatarBot
                contentView.backgroundColor = .secondaryMessageBackground
            }
        }
    }
    
    private var botPlaceholder = "Bot is contemplating"
    private var trailingDot = "."
    private var maxTrailingDots = 3
    private lazy var placeholders: [String] = {
        guard !trailingDot.isEmpty else { return [] }
        var dots = ""
        var placeholders: [String] = []
        for _ in 1...maxTrailingDots {
            dots += trailingDot
            placeholders.append(botPlaceholder + dots)
        }
        return placeholders
    }()
    private var trailingDotStep = 0
    
    private lazy var hStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 10
        stack.alignment = .top
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.isLayoutMarginsRelativeArrangement = true
        stack.directionalLayoutMargins = .init(top: 10, leading: 10, bottom: 10, trailing: 10)
        return stack
    }()

    lazy var roleView: UIImageView = {
        let v = UIImageView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 50).isActive = true
        v.widthAnchor.constraint(equalToConstant: 50).isActive = true
        return v
    }()
    
    lazy var messageTextView: UITextView = {
        let textView = UITextView()
        textView.delegate = self
        textView.textColor = .primaryText
        textView.backgroundColor = .clear
        textView.isEditable = false
        textView.isSelectable = true
        textView.isScrollEnabled = false
        textView.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        textView.textContainerInset = .init()
        return textView
    }()
    
    lazy var copyTextButton: UIButton = {
        let button = UIButton(type: .system)
        button.tintColor = .primaryButton
        button.alpha = 0
        button.setImage(UIImage(named: "copy")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 20).isActive = true
        button.widthAnchor.constraint(equalToConstant: 20).isActive = true
        button.addTarget(self, action: #selector(copyTextContent(_:)), for: .touchUpInside)
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureUI()
    }

    func setupUserMessage(_ message: Message) {
        self.message = message
        messageTextView.text = message.content
        showCopyTextButton()
    }
    
    func setupBotMessage(_ message: Message?, isAnimateText: Bool) {
        self.message = message

        Task { [weak self] in
            guard let strongSelf = self else { return }
            
            guard let message else {
                strongSelf.messageTextView.text = strongSelf.placeholders.first
                strongSelf.animateTrailingDotsIfNeeded()
                return
            }
            
            strongSelf.cancelTextAnimation()
            strongSelf.messageTextView.text = ""
            strongSelf.showCopyTextButton(isAnimate: true)
            guard isAnimateText else {
                strongSelf.messageTextView.text = message.content
                return
            }
            
            await MainActor.run {
                strongSelf.animateText(message.content)
            }
        }
    }
    
    @objc private func copyTextContent(_ sender: UIButton) {
        UIPasteboard.general.string = self.message?.content
        ToastManager.shared.addToastMessage("Copied.")
    }
}

extension ChatroomMessageCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if let deletate = cellDelegate {
            let size = messageTextView.bounds.size
            let sizeShouldFit = CGSize(width: size.width, height: .greatestFiniteMagnitude)
            deletate.updateHeightOfCell(self, fit: sizeShouldFit)
        }
    }
}


extension ChatroomMessageCell {
    private func configureUI() {
        contentView.addSubview(hStack)
        NSLayoutConstraint.activate([
            hStack.topAnchor.constraint(equalTo: contentView.topAnchor),
            hStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            hStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            hStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
        
        let leadingHStack = UIStackView()
        leadingHStack.alignment = .top
        leadingHStack.setContentHuggingPriority(.required, for: .horizontal)
        leadingHStack.setContentCompressionResistancePriority(.dragThatCanResizeScene, for: .vertical)
        leadingHStack.addArrangedSubview(roleView)
        hStack.addArrangedSubview(leadingHStack)
  
        let centerHStack = UIStackView()
        centerHStack.alignment = .center
        centerHStack.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        centerHStack.addArrangedSubview(messageTextView)
        hStack.addArrangedSubview(centerHStack)
        
        let trailingHStack = UIStackView()
        trailingHStack.addArrangedSubview(copyTextButton)
        hStack.addArrangedSubview(trailingHStack)
    }
    
    private func showCopyTextButton(isAnimate: Bool = false) {
        guard isAnimate else {
            copyTextButton.alpha = 1
            return
        }
        copyTextButton.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.copyTextButton.alpha = 1
            }) { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.copyTextButton.isEnabled = true
            }
        }
    }
}

//  MARK: - Animations
extension ChatroomMessageCell {
    func cancelTextAnimation() {
        cancellables.forEach({ $0.cancel() })
    }
    
    private func animateTrailingDotsIfNeeded() {
        guard !trailingDot.isEmpty else { return }

        let timerPublisher = Timer.publish(every: 0.8, on: .main, in: .default)
            .autoconnect()
            .map({ [weak self] _ in
                guard let strongSelf = self else { return 0 }
                strongSelf.trailingDotStep += 1
                if strongSelf.trailingDotStep >= strongSelf.maxTrailingDots {
                    strongSelf.trailingDotStep = 0
                }
                return strongSelf.trailingDotStep
            })
        
        timerPublisher
            .sink(receiveCompletion: { _ in
                print("dot publisher completed")
            }, receiveValue: { [weak self] index in
                guard let strongSelf = self else { return }
                strongSelf.messageTextView.text = strongSelf.placeholders[safe: index]
            })
            .store(in: &cancellables)
    }
    
    func animateText(_ text: String) {
        let tempArr = Array(text).compactMap({ String($0) })
        var index: Int = 0
        let timerPublisher = Timer.publish(every: 0.02, on: .main, in: .default)
            .autoconnect()
            .prefix(tempArr.count)

        timerPublisher
            .sink(receiveCompletion: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.cellDelegate?.updateHeightCompleted(strongSelf)
            }, receiveValue: { [weak self] _ in
                guard let strongSelf = self else { return }
                if let s = tempArr[safe: index] {
                    strongSelf.messageTextView.text += s
                    strongSelf.textViewDidChange(strongSelf.messageTextView)
                }
                index += 1
            })
            .store(in: &cancellables)
    }
}
