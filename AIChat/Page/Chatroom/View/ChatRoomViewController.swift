//
//  ChatRoomViewController.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import UIKit
import Combine

class ChatRoomViewController: BaseViewController {

    private let viewModel: ChatroomViewModel
    private var indexPathDisplayed: IndexPath?
    
    private let vStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 10
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.isLayoutMarginsRelativeArrangement = true
        stack.directionalLayoutMargins = .init(top: 15, leading: 15, bottom: 15, trailing: 15)
        return stack
    }()
    
    lazy var chatroomTableView: UITableView = {
        let tb = UITableView()
        tb.allowsSelection = false
        tb.translatesAutoresizingMaskIntoConstraints = false
        tb.dataSource = self
        tb.delegate = self
        tb.register(ChatroomMessageCell.self, forCellReuseIdentifier: "ChatroomMessageCell")
        return tb
    }()

    lazy var inputTextView: UITextView = {
        let textView = UITextView()
        textView.textColor = .primaryText
        textView.backgroundColor = .primaryBackground
        textView.delegate = self
        textView.isScrollEnabled = false
        textView.sizeToFit()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textContainerInset = .init(top: 10, left: 10, bottom: 10, right: 10)
        textView.makeCorners()
        textView.layer.borderColor = UIColor.primaryText.cgColor
        textView.layer.borderWidth = 1
        return textView
    }()
    
    lazy var submitButton: BaseButton = {
        let btn = BaseButton()
        btn.addTarget(self, action: #selector(submit), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.makeCorners()
        return btn
    }()
    
    lazy var emptyChatLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .gray
        lbl.text = "There's nothing here to see.".localized()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    private var vStackBottomContraints: NSLayoutConstraint?
    
    init(viewModel: ChatroomViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        dismissKeyboard()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        bindViewModel()
        observeKeyboard()
        let tapDismissKeyboard = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapDismissKeyboard)
    }
    
    private func observeKeyboard() {
        keyboardSizePublisher()
            .receive(on: DispatchQueue.main)
            .sink { [weak self] keyboardSize in
                guard let strongSelf = self else { return }
                strongSelf.animteToShowKeyboard(keyboardHeight: keyboardSize.height)
                strongSelf.view.layoutIfNeeded()
            }
            .store(in: &viewModel.cancellables)
    }
    
    @objc private func submit() {
        dismissKeyboard()
        viewModel.submitMessage()
        inputTextView.text.removeAll()
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func animteToShowKeyboard(keyboardHeight: CGFloat) {
        vStackBottomContraints?.constant = keyboardHeight > 0 ? -keyboardHeight : 0
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 500, initialSpringVelocity: 0) {[weak self] in
            guard let strongSelf = self else { return }
            strongSelf.vStackView.layoutIfNeeded()
        }
    }
}

//  MARK: - Bind ViewModel
extension ChatRoomViewController {
    private func bindViewModel() {
        
        Publishers.CombineLatest(viewModel.$input, viewModel.$chatState)
            .receive(on: RunLoop.main)
            .map({ (input, chatState) -> Bool in
                return !input.isNilOrEmpty && chatState.isTyping
            })
            .sink { [weak self] isSubmitEnable in
                guard let strongSelf = self else { return }
                strongSelf.submitButton.isEnabled = isSubmitEnable
            }
            .store(in: &viewModel.cancellables)
        
        viewModel.$chatState
            .receive(on: RunLoop.main)
            .sink { [weak self] chatState in
                guard let strongSelf = self else { return }
                switch chatState {
                case .typing:
                    strongSelf.submitButton.setTitle("Submit".localized(), for: .normal)
                case .receiving:
                    strongSelf.submitButton.setTitle("Waitting for respond".localized(), for: .normal)
                case .responding:
                    strongSelf.submitButton.setTitle("Bot is responding".localized(), for: .normal)
                }
            }
            .store(in: &viewModel.cancellables)
        
        viewModel.$messagePairs
            .receive(on: RunLoop.main)
            .sink { [weak self] receivedMessages in
                guard let strongSelf = self else { return }
                strongSelf.emptyChatLabel.isHidden = !receivedMessages.isEmpty
                guard !receivedMessages.isEmpty else { return }
                
                if let lastBotMessage = receivedMessages.last?.bot,
                   lastBotMessage.role.isAssistant,
                   !lastBotMessage.content.isEmpty {
                    let lastSectionIndex = strongSelf.chatroomTableView.numberOfSections - 1
                     let lastRowIndex = strongSelf.chatroomTableView.numberOfRows(inSection: lastSectionIndex) - 1
                    if let cell = strongSelf.chatroomTableView.cellForRow(at: IndexPath(row: lastRowIndex, section: lastSectionIndex)) as? ChatroomMessageCell {
                        cell.setupBotMessage(lastBotMessage, isAnimateText: true)
                     }
                } else {
                    strongSelf.chatroomTableView.performBatchUpdates({
                        let indexSet = IndexSet(integer: receivedMessages.count - 1)
                        strongSelf.chatroomTableView.insertSections(indexSet, with: .top)
                    }, completion: { _ in
                        DispatchQueue.main.async {
                            strongSelf.chatroomTableView.scrollToBottom()
                        }
                    })
                }
            }
            .store(in: &viewModel.cancellables)
        
        viewModel.$error
            .receive(on: RunLoop.main)
            .compactMap({ $0 })
            .sink { error in
                print(error)
            }
            .store(in: &viewModel.cancellables)
    }
    
    func keyboardSizePublisher() -> AnyPublisher<CGRect, Never> {
        return Publishers.MergeMany(
            NotificationCenter.default.publisher(for: UIResponder.keyboardWillShowNotification)
                .compactMap({ ($0.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue }),
            NotificationCenter.default.publisher(for: UIResponder.keyboardWillHideNotification)
                .compactMap({ _ in CGRect.zero })
        )
        .eraseToAnyPublisher()
    }
}

//  MARK: - UI
extension ChatRoomViewController {
    private func configureUI() {
        configureTitle()
        
        view.addSubview(vStackView)
        NSLayoutConstraint.activate([
            vStackView.topAnchor.constraint(equalTo: view.topAnchor),
            vStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            vStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        vStackBottomContraints = vStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        vStackBottomContraints?.isActive = true
        
        vStackView.addArrangedSubview(chatroomTableView)
        chatroomTableView.addSubview(emptyChatLabel)
        NSLayoutConstraint.activate([
            emptyChatLabel.topAnchor.constraint(equalTo: chatroomTableView.topAnchor, constant: 15),
            emptyChatLabel.centerXAnchor.constraint(equalTo: chatroomTableView.centerXAnchor)
        ])

        vStackView.addArrangedSubview(inputTextView)
        inputTextView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        inputTextView.heightAnchor.constraint(lessThanOrEqualToConstant: 200).isActive = true

        vStackView.addArrangedSubview(submitButton)
        submitButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    private func configureTitle() {
        guard OpenAIChatManager.default.isAPIKeyExist else {
            title = "Invalid Open AI's API Key".localized()
            return
        }
        title = OpenAIChatManager.default.currentModel.string
    }
}

//  MARK:
extension ChatRoomViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        viewModel.input = textView.text
    }
}

//  MARK: - UITableViewDataSource
extension ChatRoomViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.messagePairs.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let messagePair = viewModel.messagePairs[safe: indexPath.section] else { return UITableViewCell() }
        let cell = ChatroomMessageCell()
        cell.cellDelegate = self
        
        switch indexPath.row {
        case 0:
            cell.setupUserMessage(messagePair.user)
        case 1:
            var isAnimateMessage = false
            if let indexPathDisplayed {
                isAnimateMessage = indexPathDisplayed.section < indexPath.section
            } else {
                isAnimateMessage = true
            }
            cell.setupBotMessage(messagePair.bot, isAnimateText: isAnimateMessage)
        default:
            return cell
        }
        return cell
    }
}

extension ChatRoomViewController: GrowingCellProtocol {
    func updateHeightOfCell(_ cell: UITableViewCell, fit size: CGSize) {
        let newSize = chatroomTableView.sizeThatFits(size)
        guard size != newSize else { return }

        UIView.setAnimationsEnabled(false)
        chatroomTableView.performBatchUpdates(nil)
        UIView.setAnimationsEnabled(true)
        chatroomTableView.scrollToBottom()
    }
    
    func updateHeightCompleted(_ cell: UITableViewCell) {
        guard let indexPath = chatroomTableView.indexPath(for: cell) else { return }
        self.indexPathDisplayed = indexPath
        self.viewModel.setChatState(to: .typing)
    }
}

//  MARK: - UITableViewDelegate
extension ChatRoomViewController: UITableViewDelegate {
    
}
