//
//  ChatroomViewModel.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import Foundation
import Combine

typealias MessagePair = (user: Message, bot: Message?)

enum ChatState {
    case typing
    case receiving
    case responding
    
    var isTyping: Bool { self == .typing }
    var isReceiving: Bool { self == .receiving }
    var isResponding: Bool { self == .responding }
}

class ChatroomViewModel {
    
    @Published var input: String?
    @Published var messagePairs: [MessagePair] = []
    @Published private(set) var chatState: ChatState = .typing
    @Published var error: Error?
    
    private let roleDescription: String = "You are a helpful assistant.".localized()
    
    var cancellables: Set<AnyCancellable> = []
    
    func setChatState(to newChatState: ChatState) {
        chatState = newChatState
    }
}

//  MARK: - Handle Message
extension ChatroomViewModel {
    
    func submitMessage() {
        Task { [weak self] in
            guard let strongSelf = self else { return }
            guard let userInput = strongSelf.input else { return }
            await MainActor.run {
                strongSelf.chatState = .receiving
                if strongSelf.error != nil {
                    strongSelf.error = nil
                }
            }
            do {
                let message = Message(role: .user, content: userInput)
                let pair = MessagePair(user: message, bot: nil)
                strongSelf.messagePairs.append(pair)
                
                #if targetEnvironment(simulator)
                let receivedMessage = try await strongSelf.makeMockData().trim()
                #else
                let receivedMessage = try await strongSelf.sendMessages([message]).trim()
                #endif

                strongSelf.chatState = .responding
                insertBotMessageToLastPair(botMessage: receivedMessage)
            } catch {
                strongSelf.handleError(error)
            }
            
            await MainActor.run {
                strongSelf.input = nil
            }
        }
    }

    private func sendMessages(_ messages: [Message]) async throws -> Message {
        var bufferMessages = messages
        if self.messagePairs.count <= 1 {
            bufferMessages.insert(Message.makeSystemMessage(self.roleDescription), at: 0)
        }
        return try await OpenAIChatManager.default.submitMessages(bufferMessages)
    }

    private func insertBotMessageToLastPair(botMessage: Message) {
        var tempMessagePair = messagePairs
        guard var lastPair = tempMessagePair.popLast() else { return }
        lastPair.bot = botMessage
        tempMessagePair.append(lastPair)
        messagePairs = tempMessagePair
    }
    
    private func handleError(_ error: Error) {
        var content: String = ""
        if let apiError = error as? OpenAPIError {
            var code = ""
            if let apiErrorCode =  apiError.code {
                code = "\n\ncode: \(apiErrorCode)"
            }
            content = "⚠️ OpenAPIError"
            content += code
            content += "\n\ntype: \(apiError.type)"
            if let param = apiError.param {
                content += "\n\nparam: \(param)"
            }
            content += "\n\nmessage:\n\(apiError.message)"
        } else {
            content = "Error: \(error)"
        }
        let errorMessage = Message(role: .assistant, content: content)
        insertBotMessageToLastPair(botMessage: errorMessage)
        self.error = error
    }
}

//  MARK: - Dev and Test
private extension ChatroomViewModel {
    private func makeMockData() async throws -> Message {
        try await sleep(3)
        return Message(role: .assistant, content: "test 1\ntest 2\ntest 3\ntest 4\ntest 5")
    }
    
    private func sleep(_ second: UInt64) async throws {
        print("will sleep for \(second) sec.")
        try await Task.sleep(nanoseconds: second * 1_000_000_000)
        print("sleep end.")
    }
}

private extension Message {
    static func makeSystemMessage(_ content: String) -> Message {
        Message(role: .system, content: content)
    }
    
    func trim() -> Message {
        var trimmedContent = self.content
        while trimmedContent.hasPrefix("\n") {
            trimmedContent = String(trimmedContent.dropFirst())
        }
        return Message(role: self.role, content: trimmedContent)
    }
}
