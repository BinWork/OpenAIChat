//
//  HomeViewController.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import UIKit

class HomeViewController: UIViewController {

    private var categories: [HomeCategory] {
        [HomeCategory.prodAIChatroom]
    }
    
    lazy var tableView: UITableView = {
        let tb = UITableView()
        tb.dataSource = self
        tb.delegate = self
        tb.translatesAutoresizingMaskIntoConstraints = false
        tb.register(HomeTableViewCell.self, forCellReuseIdentifier: "HomeTableViewCell")
        return tb
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
}

extension HomeViewController {
    private func configureUI() {
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
}

//  MARK: - UITableViewDataSource
extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let category = categories[indexPath.section]
        let cell = HomeTableViewCell()
        cell.setupTitle(category.title)
        return cell
    }
}

//  MARK: - UITableViewDelegate
extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let index = indexPath.section
//        guard let category = categories[safe: index] else { return }
//        switch category {
//        case .devAIChatroom:
//            let vc = ChatroomDevViewController()
//            navigationController?.pushViewController(vc, animated: true)
//        case .prodAIChatroom:
//            let vm = ChatroomViewModel()
//            let vc = ChatRoomViewController(viewModel: vm)
//            navigationController?.pushViewController(vc, animated: true)
//            break
//        }
        let vm = ChatroomViewModel()
        let vc = ChatRoomViewController(viewModel: vm)
        navigationController?.pushViewController(vc, animated: true)
    }
}

private enum HomeCategory {
    case devAIChatroom
    case prodAIChatroom
    
    static var allCases: [HomeCategory] {
        [.prodAIChatroom, .devAIChatroom]
    }
    
    var title: String {
        switch self {
        case .devAIChatroom:
            return "develop"
        case .prodAIChatroom:
            return "production"
        }
    }
}
