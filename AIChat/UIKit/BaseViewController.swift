//
//  BaseViewController.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import UIKit

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .primaryBackground
    }
}
