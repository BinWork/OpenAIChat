//
//  BaseNavigationViewController.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import UIKit

class BaseNavigationViewController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .primaryBackground
//        navigationBar.barTintColor = .black
//        navigationBar.tintColor = .black
    }
}
