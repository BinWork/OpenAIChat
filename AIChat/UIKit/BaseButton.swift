//
//  BaseButton.swift
//  AIChat
//
//  Created by bin on 2023/3/12.
//

import UIKit

class BaseButton: UIButton {
    
    private var layoutedSize = CGSize.zero

    var isAnimate: Bool = true
    
    override var isEnabled: Bool {
        didSet {
            updateUI(isEnabled)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureUI()
    }
    
    private func configureUI() {
        backgroundColor = .primaryButton
        setTitleColor(.primaryBackground, for: .normal)
    }
    
    private func updateUI(_ flag: Bool) {
        guard isAnimate else { return }
        UIView.animate(withDuration: 0.08) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.alpha = flag ? 1.0 : 0.4
        }
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        contentEdgeInsets = .init(top: 0, left: 12, bottom: 0, right: 12)
        backgroundColor = .primaryButton
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if let previousTraitCollection = previousTraitCollection,
           previousTraitCollection.preferredContentSizeCategory != traitCollection.preferredContentSizeCategory {
            transform = .identity
            layer.removeAllAnimations()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        animateButton(true)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        animateButton(false)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        animateButton(false)
    }
    
    private func animateButton(_ highlight: Bool) {
        guard isEnabled else { return }
        UIView.animate(withDuration: 0.08) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.transform = highlight
            ? CGAffineTransform(scaleX: 0.95, y: 0.95)
            : .identity
            
            strongSelf.alpha = highlight ? 0.85 : 1.0
        }
    }
}
